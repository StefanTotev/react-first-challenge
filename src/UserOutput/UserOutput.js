import React from 'react';
import './UserOutput.css';

const UserOutput = ( props ) => {
    return (
        <div>
            <p className="p1">You are logged as {props.username}</p>
            <p className="p2">You are logged at {Date.now()}</p>
        </div>
    );
};

export default UserOutput;