import React from 'react';
import './UserInput.css';

const UserInput = ( props ) => {
    const style = {
        border: '1px solid red',
    };

    return (
        <input type="text" onChange={props.changeEvent} value={props.oldName} style={style} />
    );
};

export default UserInput;