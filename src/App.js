import React, {Component} from 'react';
import './App.css';
import UserOutput from './UserOutput/UserOutput';
import UserInput from './UserInput/UserInput';

class App extends Component {
    state = {
        username: 'relax4o'
    };

    changeUsernameHandler = ( event ) => {
        this.setState({
            username: event.target.value
        });
    };

    render () {
        return (
            <div className="App">
                <UserOutput username={this.state.username} />
                <UserInput changeEvent={this.changeUsernameHandler} oldName={this.state.username}/>
            </div>
        );
    }
}

export default App;
